@php
$path = explode('/', request()->path());
$path[1] = (array_key_exists(1, $path)> 0)?$path[1]:'';
$path[2] = (array_key_exists(2, $path)> 0)?$path[2]:'';
$path[0] = ($path[0] === '')?'documents':$path[0];


@endphp

<aside id="sidebar-left" class="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            Menu
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
            data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <li class="{{ ($path[0] === 'user')?'nav-active':'' }}">
                        <a class="nav-link" href="{{route('user_index')}}">
                            <i class="fas fa-receipt" aria-hidden="true"></i>
                            <span>Mis Datos</span>
                        </a>
                    </li>

                    @if(auth()->user()->name == 'admin')
                    <li class="{{($path[0] === 'padron_index') ? 'nav-active' : ''}}">
                        <a class="nav-link" href="{{route('padron_index')}}">
                            <i class="fas fa-receipt" aria-hidden="true"></i>
                            <span>Carga de Padron</span>
                        </a>
                    </li>
                    @endif


                </ul>
            </nav>
        </div>
        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');
                    sidebarLeft.scrollTop = initialPosition;
                }
            }

        </script>
    </div>
</aside>
